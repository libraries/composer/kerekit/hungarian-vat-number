<?php

use PHPUnit\Framework\TestCase;
use Kerekit\HungarianVatNumber\{Exception,VatNumber};

final class VatNumberTest extends TestCase
{
    public function testCanBeCreatedFromValidVatNumber ()
    {
        $this->assertInstanceOf (
            VatNumber::class,
            new VatNumber ('12345676-1-41')
        );
    }

    public function testCannotBeCreatedWithInvalidCheckDigit ()
    {
        $this->expectException (Exception::class);
        new VatNumber ('12345678-1-41');
    }
}
