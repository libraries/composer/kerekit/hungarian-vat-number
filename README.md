# Hungarian VAT number class
The `VatNumber` class parses a Hungarian VAT number in its local (not EU) format
and throws an exception when the number is invalid.

## Features (bugs?)
- It checks validity against the **check digit**.
- Provides enums for **region and VAT codes**. These are used for validation
  and may help with identification too.
- The input is only accepted in a single **strict format**: `^\d{8}-\d-\d{2}$`
  (hyphens are required, no leading or trailing whitespace/junk is tolerated).

## Examples

```php
<?php

use Kerekit\HungarianVatNumber\{Exception,RegionCode,VatCode,VatNumber};

// Init a valid VAT number
$valid = new VatNumber ('12345676-1-41');

// Display it as string
echo "The VAT number is: $valid\n"; // The VAT number is: 12345676-1-41

// Check region
if ($valid->regionCode == RegionCode::ESZAK_BUDAPEST) {
    echo "The VAT number is related to Northern Budapest region.\n";
}

// Check VAT status
if ($valid->vatCode == VatCode::AAM) {
    echo "The owner of the VAT number is VAT exempt.\n";
}

// Trying to add a VAT number with wrong check-digit results in Exception
try {
    $invalid = new VatNumber ('12345678-1-41');
} catch (Exception $e) {
    $errno = $e->getCode ();
    if ($errno == Exception::INVALID_CHECK_DIGIT) {
        echo "The given check digit is wrong. Please try to type it again.\n";
    } else {
        echo "Please type the VAT number in the following regex format: ";
        echo VatNumber::FULL_PATTERN;
        echo "\n";
    }
    exit (1);
} catch (\Exception $e) {
    echo "An unexpected error occured.\n";
    exit (1);
}
```
