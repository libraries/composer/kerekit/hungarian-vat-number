<?php

namespace Kerekit\HungarianVatNumber;

class Exception extends \Exception
{
    public const INVALID_PATTERN = 1;
    public const INVALID_CHECK_DIGIT = 2;
    public const INVALID_VAT_CODE = 4;
    public const INVALID_REGION_CODE = 8;
}
