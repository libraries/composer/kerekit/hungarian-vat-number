<?php

namespace Kerekit\HungarianVatNumber;

class VatNumber
{
    public const FULL_PATTERN = '/^(\d{8})-(\d)-(\d{2})$/';
    public const ID_PATTERN = '/^(\d{7})(\d)$/';

    public string $id;
    public VatCode $vatCode;
    public RegionCode $regionCode;

    public function __construct (string $vatNumber)
    {
        $match = preg_match (self::FULL_PATTERN, $vatNumber, $matches);
        if ($match !== 1) {
            $msg = "VAT number doesn't match pattern: '" . self::FULL_PATTERN . "'";
            throw new Exception ($msg, Exception::INVALID_PATTERN);
        }

        // Init no-error code
        $errCode = 0;

        // Validate ID
        if (self::isCheckDigitValid ($matches [1]) === true) {
            $this->id = $matches [1];
        } else {
            $errCode += Exception::INVALID_CHECK_DIGIT;
        }

        // Validate VAT code
        try {
            $this->vatCode = new VatCode ($matches [2]);
        } catch (\UnexpectedValueException $e) {
            $errCode += Exception::INVALID_VAT_CODE;
        }

        // Validate region code
        try {
            $this->regionCode = new RegionCode ($matches [3]);
        } catch (\UnexpectedValueException $e) {
            $errCode += Exception::INVALID_REGION_CODE;
        }

        // Throw exception
        if ($errCode) {
            $msg = "VAT number failed to pass validations (see code).";
            throw new Exception ($msg, $errCode);
        }
    }

    public function __toString (): string
    {
        return "$this->id-$this->vatCode-$this->regionCode";
    }

    /** @throws \Exception when receiving invalid format */
    public function isCheckDigitValid (string $id): bool
    {
        $sum = 9 * $id [0]
             + 7 * $id [1]
             + 3 * $id [2]
             + 1 * $id [3]
             + 9 * $id [4]
             + 7 * $id [5]
             + 3 * $id [6]
             ;
        $checkDigit = (10 - $sum % 10) % 10;
        return (int) $id [7] === $checkDigit;
    }
}
