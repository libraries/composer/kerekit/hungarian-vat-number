<?php

namespace Kerekit\HungarianVatNumber;

class RegionCode extends \MyCLabs\Enum\Enum
{
    /**
     * See list of region codes at:
     * https://nav.gov.hu/nav/adatbazisok/adatbleker/afaalanyok/afaalany_taj.html
     */
    public const BARANYA                  = '02';
    public const BARANYA_2                = '22';
    public const BACS                     = '03';
    public const BACS_2                   = '23';
    public const BEKES                    = '04';
    public const BEKES_2                  = '24';
    public const BORSOD_ABAUJ_ZEMPLEN     = '05';
    public const BORSOD_ABAUJ_ZEMPLEN_2   = '25';
    public const CSONGRAD                 = '06';
    public const CSONGRAD_2               = '26';
    public const FEJER                    = '07';
    public const FEJER_2                  = '27';
    public const GYOR_MOSON_SOPRON        = '08';
    public const GYOR_MOSON_SOPRON_2      = '28';
    public const HAJDU_BIHAR              = '09';
    public const HAJDU_BIHAR_2            = '29';
    public const HEVES                    = '10';
    public const HEVES_2                  = '30';
    public const KOMAROM_ESZTERGOM        = '11';
    public const KOMAROM_ESZTERGOM_2      = '31';
    public const NOGRAD                   = '12';
    public const NOGRAD_2                 = '32';
    public const PEST                     = '13';
    public const PEST_2                   = '33';
    public const SOMOGY                   = '14';
    public const SOMOGY_2                 = '34';
    public const SZABOLCS_SZATMAR_BEREG   = '15';
    public const SZABOLCS_SZATMAR_BEREG_2 = '35';
    public const JASZ_NAGYKUN_SZOLNOK     = '16';
    public const JASZ_NAGYKUN_SZOLNOK_2   = '36';
    public const TOLNA                    = '17';
    public const TOLNA_2                  = '37';
    public const VAS                      = '18';
    public const VAS_2                    = '38';
    public const VESZPREM                 = '19';
    public const VESZPREM_2               = '39';
    public const ZALA                     = '20';
    public const ZALA_2                   = '40';
    public const ESZAK_BUDAPEST           = '41';
    public const KELET_BUDAPEST           = '42';
    public const DEL_BUDAPEST             = '43';
    public const KAVIG                    = '44';
    public const KAVIG_2                  = '51';
}
