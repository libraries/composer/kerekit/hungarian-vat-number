<?php

namespace Kerekit\HungarianVatNumber;

class VatCode extends \MyCLabs\Enum\Enum
{
    /** @var int VAT exempt subject */
    public const AAM = '1';

    /** @var int Usual VAT applies */
    public const ALTALANOS = '2';

    /** @var int Simplified VAT applies */
    public const EVA = '3';

    /** @var int Group VAT subject 1. (who knows what it means...) */
    public const GROUP_1 = '4';

    /** @var int Group VAT subject 2. (who knows what it means...) */
    public const GROUP_2 = '5';
}
